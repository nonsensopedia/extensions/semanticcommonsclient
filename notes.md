## Data that we can retrieve

### Coordinates
In Wikidata: ? P31 Q18615777
e.g. https://www.wikidata.org/wiki/Property:P1259

* latitude
  * CM: GPSLatitude (WGS-84)
* longitude
  * CM: GPSLongitude (WGS-84)
* type
  * CM: GPSMapDatum, it should be only WGS-84
  
### Author
In CM: Artist – author name (might contain complex HTML, multiple authors etc)

WD:
* P170 – creator
    * can be a WD ID
    * Q4233718 is anonymous
    * otherwise, should have qualifiers:
        * P2093 – author name string
        * P4174 – Wikimedia username
        * P2699 – URL

### License
In CM:
* LicenseShortName – short human-readable license name
  * We should probably use that and then map it to local templates
  * Fields LicenseUrl and LicenseTerms – ??? override locals?
* Copyrighted – `True` or `False` (for public domain images)

WD:
* P275 – license
    * maps to schema: and dcterms:
    * values are subclasses of Q79719
* P6216 – copyright status
    * Q50423863 – copyrighted
    * other subclasses of Q50424085 (copyright status)

### Source
We assume everything is from Commons... for now, may want to propagate the source

Wikidata has some nice props for that, though, tempting.
* P7482 – source
    * Q66458942 – own work

### Date
In CM: DateTimeOriginal – time of creation (space-separated ISO 8601 timestamp whenever possible, but can be any other textual description of a date, possibly with HTML mixed in)

In WD:
* P571 – inception
    * values can be crazy
    * snak -> datavalue -> value -> time looks ISO enough to me
    
### Caption
In WD:
* item label – use content language with fallback