<?php

namespace SemanticCommonsClient\Cache;

use BagOStuff;

abstract class SimpleCache {

	/** @var BagOStuff */
	private $mainStash;

	/** @var Cacheable[] */
	private $cacheKeyMap;

	/** @var Cacheable[] */
	protected $set;

	/**
	 * @param BagOStuff $mainStash
	 * @param Cacheable[] $objects
	 */
	public function __construct( BagOStuff $mainStash, array $objects ) {
		$this->mainStash = $mainStash;
		$this->set = $objects;
	}

	/**
	 * Attempts to load all objects from cache.
	 *
	 * @return int Number of objects that were successfully loaded.
	 */
	public function loadCached() : int {
		$result = $this->mainStash->getMulti( array_keys( $this->getCacheKeyMap() ) );
		$count = 0;
		foreach ( $result as $key => $value ) {
			$this->getCacheKeyMap()[$key]->setFromArray( $value );
			$count++;
		}

		return $count;
	}

	/**
	 * @return mixed Should be bool, but that can cause runtime errors... WHAT?!
	 * TODO: investigate this
	 */
	public function saveToCache() {
		$toCache = [];
		foreach ( $this->getCacheKeyMap() as $key => $value ) {
			if ( $value->getCacheTimestamp() ) {
				continue;
			}
			$value->setCacheTimestamp( wfTimestampNow() );
			$toCache[$key] = $value->jsonSerialize();
		}

		return $this->mainStash->setMulti(
			$toCache,
			BagOStuff::TTL_DAY * 7, BagOStuff::WRITE_BACKGROUND
		);
	}

	/**
	 * @return Cacheable[]
	 */
	private function getCacheKeyMap() : array {
		if ( !isset( $this->cacheKeyMap ) ) {
			$this->cacheKeyMap = [];
			foreach ( $this->set as $object ) {
				$this->cacheKeyMap[$object->getCacheKey()] = $object;
			}
		}
		return $this->cacheKeyMap;
	}
}