<?php

namespace SemanticCommonsClient\Cache;

use JsonSerializable;

interface Cacheable extends JsonSerializable {

	/**
	 * Returns a cache key for the object.
	 * Includes the current serialization format version, so that cached entries in old
	 * formats are discarded automatically.
	 *
	 * @return string
	 */
	public function getCacheKey() : string;

	public function getCacheTimestamp() : ?string;

	public function setCacheTimestamp( string $timestamp ) : void;

	/**
	 * Sets data on this object from an array.
	 *
	 * @param array $a
	 */
	public function setFromArray( array $a ) : void;
}