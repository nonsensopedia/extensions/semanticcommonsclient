<?php

namespace SemanticCommonsClient\Api;

use ApiBase;
use ApiMain;
use BagOStuff;
use MediaWiki\Permissions\PermissionManager;
use MWException;
use PermissionsError;
use SemanticCommonsClient\FileDescription\FileDescription;
use SemanticCommonsClient\Services\AuthorDescriptionService;
use Title;
use Wikimedia\ParamValidator\ParamValidator;

class PurgeMetadataModule extends ApiBase {

	/** @var BagOStuff */
	private $mainStash;

	/** @var PermissionManager */
	private $permissionManager;

	/**
	 * @param ApiMain $main
	 * @param string $action
	 * @param BagOStuff $mainStash
	 * @param PermissionManager $permissionManager
	 */
	public function __construct(
		ApiMain $main,
		string $action,
		BagOStuff $mainStash,
		PermissionManager $permissionManager
	) {
		parent::__construct( $main, $action );
		$this->mainStash = $mainStash;
		$this->permissionManager = $permissionManager;
	}

	/**
	 * @inheritDoc
	 * @throws MWException
	 */
	public function execute() {
		if ( !$this->permissionManager->userHasRight(
			$this->getUser(),
			'purge-commons-metadata'
		) ) {
			throw new PermissionsError( 'purge-commons-metadata' );
		}

		$params = $this->extractRequestParams();
		$this->requireAtLeastOneParameter( $params, 'files', 'authors' );

		// ratelimit on requests
		$this->getUser()->pingLimiter( 'scc-purge-request' );
		$result = $this->getResult();
		$cacheKeys = [];

		// Files
		foreach ( $params['files'] ?? [] as $file ) {
			$title = Title::newFromText( $file );
			$v = [
				'query' => $file,
				'valid' => $title ? 1 : 0
			];

			if ( $title ) {
				$canonical = 'File:' . $title->getText();
				$v['canonical'] = $canonical;
				$fd = new FileDescription( $canonical );
				$cacheKeys[] = $fd->getCacheKey();
			}

			$result->addValue( 'files', null, $v );
		}

		// Authors
		foreach ( $params['authors'] ?? [] as $author ) {
			$cacheKeys[] = AuthorDescriptionService::getCacheKey( $author );
			$result->addValue( 'authors', null, $author );
		}

		// ratelimit on cache key count
		$this->getUser()->pingLimiter( 'scc-purge-keys', count( $cacheKeys ) );
		$result->addValue( null, 'cacheKeys', count( $cacheKeys ) );

		$this->mainStash->deleteMulti( $cacheKeys, BagOStuff::WRITE_BACKGROUND );
	}

	/**
	 * @return array[]
	 */
	public function getAllowedParams() {
		return [
			'authors' => [
				ParamValidator::PARAM_TYPE => 'string',
				ParamValidator::PARAM_ISMULTI => true,
				ParamValidator::PARAM_ISMULTI_LIMIT1 => 50,
				ParamValidator::PARAM_ISMULTI_LIMIT2 => 100,
			],
			'files' => [
				ParamValidator::PARAM_TYPE => 'string',
				ParamValidator::PARAM_ISMULTI => true,
				ParamValidator::PARAM_ISMULTI_LIMIT1 => 50,
				ParamValidator::PARAM_ISMULTI_LIMIT2 => 100,
			]
		];
	}

	/**
	 * @return bool
	 */
	public function isWriteMode() : bool {
		return true;
	}

	/**
	 * @return bool
	 */
	public function mustBePosted() : bool {
		return true;
	}
}