<?php

namespace SemanticCommonsClient\Sparql;

use EasyRdf\Graph;
use EasyRdf\Sparql\Client;
use EasyRdf\Sparql\Result;
use Exception;
use FileBackendError;
use MediaWiki\Logger\LoggerFactory;

class QueryExecutor {
	private const SPARQL_BASE_DIR = __DIR__ . '/../../resources/sparql/';

	/** @var string */
	private $query;

	/** @var string */
	private $endpoint;

	/**
	 * @param string $endpoint
	 * @param string $filename
	 *
	 * @throws FileBackendError
	 */
	public function __construct( string $endpoint, string $filename ) {
		$query = file_get_contents( self::SPARQL_BASE_DIR . $filename );
		if ( !$query ) {
			throw new FileBackendError();
		}
		$this->query = $query;
		$this->endpoint = $endpoint;
	}

	/**
	 * @param string $param
	 * @param string $value Escaping the value is the caller's responsibility.
	 */
	public function substituteParamWithValue( string $param, string $value ) : void {
		$this->query = str_replace( "<<$param>>", $value, $this->query );
	}

	/**
	 * @param string $param
	 * @param array $array Should be a 1D array, e.g.:
	 *   [ 'v1', 'v2', 'v3' ]
	 */
	public function substituteParamWithArray1D( string $param, array $array ) : void {
		$this->substituteParamWithValue( $param, implode( "\n", $array ) );
	}

	/**
	 * @param string $param
	 * @param array $array Should be a 2D array, e.g.:
	 *   [ [ 'p1v1', 'p2v1' ], [ 'p1v2', 'p2v2' ], [ 'p1v3', 'p2v3' ] ]
	 */
	public function substituteParamWithArray2D( string $param, array $array ) : void {
		$value = '';
		foreach ( $array as $row ) {
			$value .= '(' . implode( ' ', $row ) . ")\n";
		}
		$this->substituteParamWithValue( $param, $value );
	}

	/**
	 * @return Graph|Result|null
	 */
	public function execute() {
		$client = new Client( $this->endpoint );
		try {
			return $client->query( $this->query );
		} catch ( Exception $ex ) {
			LoggerFactory::getInstance( 'SparqlQuery' )
				->error( 'Failed to execute SPARQL query', [
					'query' => $this->query,
					'endpoint' => $this->endpoint,
					'reason' => $ex->getMessage(),
					'trace' => $ex->getTraceAsString(),
				] );
			return null;
		}
	}
}