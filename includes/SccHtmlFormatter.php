<?php

namespace SemanticCommonsClient;

use RemexHtml\HTMLData;
use RemexHtml\Serializer\HtmlFormatter;
use RemexHtml\Serializer\SerializerNode;

class SccHtmlFormatter extends HtmlFormatter {
	public function element( SerializerNode $parent, SerializerNode $node, $contents ) {
		// Strip everything that is invisible
		if ( $node->namespace === HTMLData::NS_HTML ) {
			if ( preg_match( '/display:\s*none/', $node->attrs['style'] ?? '' ) ) {
				return '';
			}
		}

		return $contents;
	}
}