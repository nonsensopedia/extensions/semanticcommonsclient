<?php

namespace SemanticCommonsClient;

use SemanticCommonsClient\Services\SccServices;
use Title;

/**
 * Debug stuff.
 *
 * Unit tests aren't free, you know? I for one can't afford them.
 */
class SomeTestCode {

	public static function test1($ignoreCache = false) {
		$names = [
			/*'File:Politechnika Warszawska surveying.jpg',
			"Gérôme - L'entrée du Christ à Jérusalem - cadre.jpg",
			'File:Matejko Self-portrait.jpg',
			'File:Würfelzucker -- 2018 -- 3564.jpg',
			'03-07-dagebuell-by-RalfR-157.jpg',
			'File:Tired 20-year-old cat.jpg',
			'Nonsensopedia logo.svg',*/
			'File:Uncyclopedia_logo_notext.svg',
		];

		$fdc = SccServices::getFileDescriptionService();

		$targets = array_map(
			function ( $s ) {
				return Title::newFromText( $s );
			},
			$names
		);

		return $fdc->getDataForFiles( $targets, $ignoreCache );
	}

	public static function test2($ignoreCache = false) {
		$names = [
			'Q42',
			'Douglas Adams',
			'wikipedia:pl:Jan Matejko',
			'en:Bob Ross',
			'cs:Angela Merkelová' // yes, they actually call her that
		];

		$adc = SccServices::getAuthorDescriptionService();

		return $adc->getDataForAuthors( $names, $ignoreCache );
	}
}