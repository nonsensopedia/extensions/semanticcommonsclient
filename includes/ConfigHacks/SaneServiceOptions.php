<?php

namespace SemanticCommonsClient\ConfigHacks;

use Config;
use InvalidArgumentException;
use Wikimedia\Assert\Assert;

/**
 * Taken from MediaWiki Core and then ruthlessly mutilated.
 * RIP.
 *
 * @sane since it was rewritten
 */
class SaneServiceOptions implements Config {

	private $keys;
	private $options = [];

	/**
	 * @param string[] $keys Which keys to extract from $sources
	 * @param Config|array ...$sources Each source is either a Config object or an array. If the
	 *  same key is present in two sources, the first one takes precedence. Keys that are not in
	 *  $keys are ignored.
	 * @throws InvalidArgumentException if one of $keys is not found in any of $sources
	 */
	public function __construct( array $keys, ...$sources ) {
		$this->keys = $keys;
		foreach ( $keys as $key ) {
			foreach ( $sources as $source ) {
				if ( $source instanceof Config ) {
					if ( $source->has( $key ) ) {
						$this->options[$key] = $source->get( $key );
						continue 2;
					}
				} else {
					if ( array_key_exists( $key, $source ) ) {
						$this->options[$key] = $source[$key];
						continue 2;
					}
				}
			}
			throw new InvalidArgumentException( "Key \"$key\" not found in input sources" );
		}
	}

	/**
	 * Assert that the list of options provided in this instance exactly match $expectedKeys,
	 * without regard for order.
	 *
	 * @param string[] $expectedKeys
	 */
	public function assertRequiredOptions( array $expectedKeys ) {
		if ( $this->keys !== $expectedKeys ) {
			$extraKeys = array_diff( $this->keys, $expectedKeys );
			$missingKeys = array_diff( $expectedKeys, $this->keys );
			Assert::precondition( !$extraKeys && !$missingKeys,
				(
				$extraKeys
					? 'Unsupported options passed: ' . implode( ', ', $extraKeys ) . '!'
					: ''
				) . ( $extraKeys && $missingKeys ? ' ' : '' ) . (
				$missingKeys
					? 'Required options missing: ' . implode( ', ', $missingKeys ) . '!'
					: ''
				)
			);
		}
	}

	/**
	 * @param string $name
	 *
	 * @return mixed
	 */
	public function get( $name ) {
		if ( !array_key_exists( $name, $this->options ) ) {
			throw new InvalidArgumentException( "Unrecognized option \"$name\"" );
		}
		return $this->options[$name];
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function has( $name ) {
		return array_key_exists( $name, $this->options );
	}
}