<?php

namespace SemanticCommonsClient\ConfigHacks;

/**
 * Trait for classes that accept SaneServiceOptions.
 * Allows for easy and consistent building and validation of SaneServiceOptions.
 */
trait Configurable {

	public abstract static function getConstructorOptions() : array;

	public static function buildServiceOptions( ...$sources ) : SaneServiceOptions {
		return new SaneServiceOptions( static::getConstructorOptions(), ...$sources );
	}

	protected static function assertRequiredOptions( SaneServiceOptions $options ) : void {
		$options->assertRequiredOptions( static::getConstructorOptions() );
	}
}