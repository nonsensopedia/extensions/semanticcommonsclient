<?php

namespace SemanticCommonsClient\ForeignApi;

use SemanticCommonsClient\FileDescription\FileDescription;
use SemanticCommonsClient\FileDescription\FileDescriptionSet;

class CommonsApiClient extends BaseCommonsApiClient {

	/**
	 * @param FileDescriptionSet $files
	 *
	 * @return void
	 */
	public function getDataForFiles( FileDescriptionSet $files ) : void {
		$filenames = $files->getNames(
			[ FileDescriptionSet::FILTER_UNCACHED_ONLY ]
		);
		$queryParams = [
			'action' => 'query',
			'format' => 'json',
			'prop' => 'imageinfo',
			'titles' => implode( '|', $filenames ),
			'iiprop' => 'extmetadata',
			'iiextmetadatalanguage' => $this->options->get( 'LanguageCode' )
		];

		$result = $this->makeRequest( $this->options->get( 'SccCommonsApiUrl' ), $queryParams );
		$data = $result->getValue()['pages'] ?? [];

		$foundFilenames = [];
		foreach ( $data as $page ) {
			$fd = $files->get( $page['title'] ?? '' );
			if ( $fd === null || array_key_exists( 'missing', $page ) ) {
				continue;
			}

			$foundFilenames[] = $fd->getFilename();
			$fd->found = true;
			$imageinfo = $page['imageinfo'][0]['extmetadata'] ?? null;
			if ( $imageinfo !== null ) {
				$this->extractMetadata( $fd, $imageinfo );
			}
		}

		foreach ( array_diff( $filenames, $foundFilenames ) as $filename ) {
			$files->get( $filename )->addWarning( 'scc-no-data-returned' );
		}
	}

	/**
	 * @param FileDescription $fd
	 * @param array $data
	 */
	private function extractMetadata( FileDescription $fd, array $data ) : void {
		// GPS
		if ( $data['GPSMapDatum']['value'] ?? '' === 'WGS-84' ) {
			$fd->location->latitude = $data['GPSLatitude']['value'] ?? null;
			$fd->location->longitude = $data['GPSLongitude']['value'] ?? null;
		}

		// Date
		$fd->dateTime = $data['DateTimeOriginal']['value'] ?? null;

		// License
		$fd->license->shortName = $data['LicenseShortName']['value'] ?? null;
		$fd->license->longName = $data['UsageTerms']['value'] ?? null;
		if ( isset( $data['Copyrighted']['value'] ) ) {
			$fd->license->isPublicDomain = $data['Copyrighted']['value'] === 'False';
		}
		$fd->license->url = $data['LicenseUrl']['value'] ?? null;

		// Author
		if ( isset( $data['Artist']['value'] ) ) {
			$val = $data['Artist']['value'];
			if ( preg_match_all( '/href="([^"]+)"/', $val, $matches ) === 1 ) {
				$fd->author->url = $matches[1][0];
			}
			$fd->author->text = $val;
		}
	}
}