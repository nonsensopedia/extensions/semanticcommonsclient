<?php

namespace SemanticCommonsClient\ForeignApi;

use BagOStuff;
use EasyRdf\Literal;
use EasyRdf\Resource;
use EasyRdf\Sparql\Result;
use SemanticCommonsClient\ConfigHacks\Configurable;
use SemanticCommonsClient\ConfigHacks\SaneServiceOptions;
use SemanticCommonsClient\FileDescription\AuthorDescription;
use SemanticCommonsClient\FileDescription\FileDescriptionSet;
use SemanticCommonsClient\FileDescription\LicenseDescriptionSet;
use SemanticCommonsClient\Sparql\QueryExecutor;
use stdClass;

class WikidataSparqlClient implements ApiClient {
	use Configurable;

	/** @var SaneServiceOptions */
	private $options;

	/** @var BagOStuff|null */
	private $mainStash;

	/** @var bool */
	private $useLicenseCache;

	public static function getConstructorOptions() : array {
		return [
			'SccWikidataQueryServiceUrl',
			'LanguageCode',
		];
	}

	/**
	 * WikidataSparqlClient constructor.
	 *
	 * @param BagOStuff $mainStash Used for license caching
	 * @param SaneServiceOptions $options
	 * @param bool $useLicenseCache
	 */
	public function __construct(
		BagOStuff $mainStash,
		SaneServiceOptions $options,
		bool $useLicenseCache = true
	) {
		self::assertRequiredOptions( $options );
		$this->options = $options;
		$this->mainStash = $mainStash;
		$this->useLicenseCache = $useLicenseCache;
	}

	/**
	 * @param FileDescriptionSet $files
	 */
	public function getDataForFiles( FileDescriptionSet $files ) : void {
		$this->doAuthors( $files );
		$this->doLicenses( $files );
	}

	/**
	 * @param string[] $authors Either WD IDs or Wikipedia interwiki links
	 *
	 * @return AuthorDescription[] [ wdId => AuthorDescription ]
	 */
	public function getAuthorData( array $authors ) : array {
		$param = [];
		$wikipediaLinkToKey = [];
		$wdIdToKey = [];

		foreach ( $authors as $author ) {
			$a = trim( $author );
			if ( $a === '' ) {
				continue;
			} elseif ( preg_match( '/^Q\d+$/', $a ) ) {
				$param[] = [ "wd:$a", '0' ];
				$wdIdToKey[$a] = $author;
			} elseif ( preg_match( '/^:?(?:[wW]ikipedia)?:?([a-z]+)?:?(.+)/', $a, $m ) ) {
				$lang = $m[1] ?: 'en';
				$title = str_replace( "'", "\'", $m[2] );
				$title = str_replace( '_', ' ', $title );
				$param[] = [ '0', "'$title'@$lang" ];
				$wikipediaLinkToKey[$lang][$title] = $author;
			}
		}

		$res = $this->queryAuthorData( $param );

		// Iterate through the results
		$foundAuthors = [];
		foreach ( $res ?: [] as $row ) {
			if ( !$row->author instanceof Resource ) {
				continue;
			}

			$id = $row->author->localName();
			$author = new AuthorDescription();
			self::fillAuthorFromWikidata( $author, $id, $row );

			if ( $row->inAuthorName instanceof Literal &&
				$row->inAuthorName->getValue() != '0'
			) {
				$key = $wikipediaLinkToKey
					[$row->inAuthorName->getLang()]
					[$row->inAuthorName->getValue()] ?? $author->wikidataId;
			} else {
				$key = $wdIdToKey[$id] ?? $author->wikidataId;
			}

			if ( array_key_exists( $key, $foundAuthors ) ) {
				continue;
			}
			$foundAuthors[$key] = $author;
		}

		return $foundAuthors;
	}

	/**
	 * @param FileDescriptionSet $fds
	 */
	private function doAuthors( FileDescriptionSet $fds ) : void {
		$wdIdToAuthor = [];
		$wikipediaLinkToAuthor = [];

		$files = $fds->getValues( [
			FileDescriptionSet::FILTER_UNCACHED_ONLY,
			FileDescriptionSet::FILTER_WITH_WIKIBASE_ID
		] );
		foreach ( $files as $file ) {
			if ( $file->author->wikidataId ) {
				$wdIdToAuthor[$file->author->wikidataId][] = $file->author;
			} elseif ( $file->author->url ) {
				list( $lang, $title ) = $this->dissectWikipediaLink( $file->author->url );
				if ( $lang !== null ) {
					$wikipediaLinkToAuthor[$lang][$title][] = $file->author;
				}
			}
		}

		// Build SPARQL param array
		$authorsParam = [];
		foreach ( $wdIdToAuthor as $wdId => $_ ) {
			$authorsParam[] = [ "wd:$wdId", '0' ];
		}
		foreach ( $wikipediaLinkToAuthor as $lang => $links ) {
			foreach ( $links as $link => $_ ) {
				$lEscaped = str_replace( "'", "\'", $link );
				$authorsParam[] = [ '0', "'$lEscaped'@$lang" ];
			}
		}

		// Execute the SPARQL query
		$res = $this->queryAuthorData( $authorsParam );

		// Iterate through the results and fill in the information
		$foundAuthors = [];
		foreach ( $res ?: [] as $row ) {
			if ( !$row->author instanceof Resource ) {
				continue;
			}
			$id = $row->author->localName();
			if ( in_array( $id, $foundAuthors ) ) {
				continue;
			}
			$foundAuthors[] = $id;
			if ( $row->inAuthorName instanceof Literal &&
				$row->inAuthorName->getValue() != '0'
			) {
				$authors = $wikipediaLinkToAuthor
					[$row->inAuthorName->getLang()]
					[$row->inAuthorName->getValue()] ?? [];
			} else {
				$authors = $wdIdToAuthor[$id] ?? [];
			}

			foreach ( $authors as $author ) {
				self::fillAuthorFromWikidata( $author, $id, $row );
			}
		}
	}

	/**
	 * @param array $authorsParam
	 *
	 * @return Result|null
	 */
	private function queryAuthorData( array $authorsParam ) : ?Result {
		if ( count( $authorsParam ) === 0 ) {
			return null;
		}

		$qe = new QueryExecutor(
			$this->options->get( 'SccWikidataQueryServiceUrl' ),
			'authors.rq'
		);
		$qe->substituteParamWithArray2D( 'authors', $authorsParam );
		$qe->substituteParamWithValue( 'lang', $this->options->get( 'LanguageCode' ) );
		return $qe->execute();
	}

	/**
	 * @param string $url
	 *
	 * @return array [$lang, $title]
	 */
	private static function dissectWikipediaLink( string $url ) : array {
		if ( preg_match(
			'/.*?:\/\/([a-z]+)\.wikipedia\.org\/wiki\/([^?]+)/',
			$url,
			$matches
		) ) {
			$lang = $matches[1];
			// Urldecode, normalize spaces in title
			$title = str_replace( '_', ' ', urldecode( $matches[2] ) );
			// Try to remove anything looking like a lang prefix
			// Assume it's lowercase and no special characters
			if ( preg_match( '/^([a-z]+):(.*)/', $title, $mPrefix ) ) {
				$lang = $mPrefix[1];
				$title = $mPrefix[2];
			}
			return [ $lang, $title ];

		} else {
			return [ null, null ];
		}
	}

	/**
	 * @param AuthorDescription $author
	 * @param string $id
	 * @param stdClass $row
	 */
	private static function fillAuthorFromWikidata(
		AuthorDescription $author,
		string $id,
		stdClass $row
	) : void {
		$author->wikidataId = $id;
		if ( isset( $row->authorLabel ) && $row->authorLabel instanceof Literal ) {
			$author->text = $row->authorLabel->getValue();
		}
		if ( isset( $row->article ) && $row->article instanceof Resource ) {
			$author->url = $row->article->getUri();
			$author->wikipediaArticleTitle = $row->articleTitle->getValue();
			$author->wikipediaArticleLang = $row->articleLang->getValue();
		}
	}

	/**
	 * @param FileDescriptionSet $fds
	 */
	private function doLicenses( FileDescriptionSet $fds ) : void {
		// Build license set
		$licenses = [];
		foreach ( $fds->getValues( [ FileDescriptionSet::FILTER_UNCACHED_ONLY ] ) as $fd ) {
			$ld = $fd->license;
			if ( $ld->wikidataId && !isset( $licenses[$ld->wikidataId] ) ) {
				$licenses[$ld->wikidataId] = clone( $ld );
			}
		}
		$lds = new LicenseDescriptionSet( $this->mainStash, $licenses );

		// Try to load them from cache
		$totalUniqueLicenses = count( $lds->getNames() );
		if ( $this->useLicenseCache ) {
			$loadedFromCache = $lds->loadCached();
			if ( $totalUniqueLicenses === $loadedFromCache ) {
				$this->updateLicensesInSet( $fds, $lds );
				return;
			}
		}

		// Build a list of license IDs to be queried
		$licensesParam = [];
		foreach ( $lds->getValues( true ) as $ld ) {
			if ( $ld->wikidataId && !in_array( $ld->wikidataId, $licensesParam ) ) {
				$licensesParam[] = "wd:$ld->wikidataId";
			}
		}
		if ( count( $licensesParam ) === 0 ) {
			return;
		}

		// Query Wikidata
		$qe = new QueryExecutor(
			$this->options->get( 'SccWikidataQueryServiceUrl' ),
			'licenses.rq'
		);
		$qe->substituteParamWithArray1D( 'licenses', $licensesParam );
		$qe->substituteParamWithValue( 'lang', $this->options->get( 'LanguageCode' ) );
		$res = $qe->execute();

		// Iterate over results
		foreach ( $res ?: [] as $row ) {
			if ( !isset( $row->license ) || !$row->license instanceof Resource ) {
				continue;
			}
			$license = $lds->getById( $row->license->localName() );
			if ( $license === null ) {
				continue;
			}

			if ( isset( $row->spdx ) ) {
				$license->spdxId = $row->spdx->getValue();
			}
			if ( isset( $row->licenseUrl ) && $row->licenseUrl instanceof Resource ) {
				$license->url = $row->licenseUrl->getUri();
			}
			if ( isset( $row->licenseShortName ) ) {
				$license->shortName = $row->licenseShortName->getValue();
			}
			// Only set the long name if it's actually longer than the short one
			if ( isset( $row->licenseLabel ) ) {
				$label = $row->licenseLabel->getValue();
				if ( strlen( $license->shortName ?: '' ) < strlen( $label ) ) {
					$license->longName = $label;
				}
			}
		}

		$this->updateLicensesInSet( $fds, $lds );
	}

	/**
	 * @param FileDescriptionSet $fds
	 * @param LicenseDescriptionSet $lds
	 */
	private function updateLicensesInSet(
		FileDescriptionSet $fds,
		LicenseDescriptionSet $lds
	) : void {
		foreach ( $fds->getValues( [ FileDescriptionSet::FILTER_UNCACHED_ONLY ] ) as $fd ) {
			if ( $fd->license->wikidataId === null ) {
				continue;
			}
			$new = $lds->getById( $fd->license->wikidataId );
			if ( $new ) {
				$fd->license = $new;
			}
		}
	}


}