<?php

namespace SemanticCommonsClient\ForeignApi;

use FormatJson;
use MediaWiki\Http\HttpRequestFactory;
use SemanticCommonsClient\ConfigHacks\Configurable;
use SemanticCommonsClient\ConfigHacks\SaneServiceOptions;
use Status;

abstract class BaseCommonsApiClient implements ApiClient {
	use Configurable;

	/** @var HttpRequestFactory */
	private $httpRequestFactory;

	/** @var SaneServiceOptions */
	protected $options;

	public static function getConstructorOptions() : array {
		return [
			'SccCommonsApiUrl',
			'LanguageCode'
		];
	}

	public function __construct(
		HttpRequestFactory $httpRequestFactory,
		SaneServiceOptions $options
	) {
		self::assertRequiredOptions( $options );
		$this->options = $options;
		$this->httpRequestFactory = $httpRequestFactory;
	}

	/**
	 * Makes a request to MediaWiki API. Automatically handles the continue param.
	 * Returns a Status, its value is the merged contents of the "query" part of the API result.
	 *
	 * @param string $endpoint
	 * @param array $params
	 *
	 * @param bool $supportContinue
	 *
	 * @return Status
	 */
	protected function makeRequest(
		string $endpoint,
		array $params,
		bool $supportContinue = true
	) : Status {
		if ( !$supportContinue ) {
			return $this->makeRequestInternal( $endpoint, $params );
		}

		$values = [];
		$continue = [];
		while ( true ) {
			$result = $this->makeRequestInternal( $endpoint, $params + $continue );
			if ( !$result->isGood() ) {
				return $result;
			}

			$newVals = $result->getValue()['query'] ?? [];
			$newCont = $result->getValue()['continue'] ?? [];
			// Either nothing was returned or we got the same result again
			if ( !$newVals || ( $continue && $newCont == $continue ) ) {
				break;
			}

			$values = array_merge_recursive( $values, $newVals );

			// No more results
			if ( !$newCont ) {
				break;
			}
			$continue = $newCont;
		}
		return Status::newGood( $values );
	}

	/**
	 * @param string $endpoint
	 * @param array $params
	 *
	 * @return Status
	 */
	private function makeRequestInternal( string $endpoint, array $params ) : Status {
		$result = $this->httpRequestFactory->post(
			$endpoint,
			[ 'postData' => $params ]
		);

		if ( $result === null ) {
			return Status::newFatal( 'scc-no-connection' );
		}

		return FormatJson::parse( $result, FormatJson::FORCE_ASSOC );
	}
}