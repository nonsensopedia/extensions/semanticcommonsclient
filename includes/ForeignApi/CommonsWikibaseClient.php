<?php

namespace SemanticCommonsClient\ForeignApi;

use SemanticCommonsClient\FileDescription\FileDescription;
use SemanticCommonsClient\FileDescription\FileDescriptionSet;
use SemanticCommonsClient\FileDescription\LicenseDescription;

class CommonsWikibaseClient extends BaseCommonsApiClient {

	/**
	 * @param FileDescriptionSet $files
	 *
	 * @return void
	 */
	public function getDataForFiles( FileDescriptionSet $files ) : void {
		$filenames = $files->getNames(
			[ FileDescriptionSet::FILTER_UNCACHED_ONLY ]
		);
		$queryParams = [
			'action' => 'wbgetentities',
			'format' => 'json',
			'languages' => $this->options->get( 'LanguageCode' ),
			'languagefallback' => 1,
			'props' => 'info|claims|labels',
			'sites' => 'commonswiki',
			'titles' => implode( '|', $filenames ),
		];

		$result = $this->makeRequest(
			$this->options->get( 'SccCommonsApiUrl' ),
			$queryParams,
			false
		);
		$data = $result->getValue()['entities'] ?? [];

		$foundFilenames = [];
		foreach ( $data as $page ) {
			$fd = $files->get( $page['title'] ?? '' );
			if ( $fd === null || array_key_exists( 'missing', $page ) ) {
				continue;
			}
			$foundFilenames[] = $fd->getFilename();
			$this->extractData( $fd, $page );
		}

		foreach ( array_diff( $filenames, $foundFilenames ) as $filename ) {
			$files->get( $filename )->addWarning( 'scc-wikibase-no-data-returned' );
		}
	}

	/**
	 * @param FileDescription $fd
	 * @param array $data
	 */
	private function extractData( FileDescription $fd, array $data ) {
		// Entity ID
		$fd->entityId = $data['id'];

		// Caption
		$labels = array_values( $data['labels'] ?? [] );
		if ( $labels ) {
			$fd->caption = $labels[0]['value'];
		}

		// Author
		if ( isset( $data['statements']['P170'][0] ) ) {
			$val = $data['statements']['P170'][0];
			$wdId = $val['mainsnak']['datavalue']['value']['id'] ?? null;
			if ( $wdId ) {
				// Author from Wikidata
				$fd->author->wikidataId = $wdId;
				if ( $wdId === 'Q4233718' ) {
					// https://www.wikidata.org/wiki/Q4233718
					$fd->author->isAnonymous = true;
				}
			} else {
				// Plainly specified author?
				$authorText = $val['qualifiers']['P2093'][0]['datavalue']['value'] ?? null;
				if ( $authorText ) {
					$fd->author->text = $authorText;
				}
				$wikimediaUsername = $val['qualifiers']['P4174'][0]['datavalue']['value'] ?? null;
				if ( $wikimediaUsername ) {
					$fd->author->wikimediaUsername = $wikimediaUsername;
				}
				$authorUrl = $val['qualifiers']['P2699'][0]['datavalue']['value'] ?? null;
				if ( $authorUrl ) {
					$fd->author->url = $authorUrl;
				}
			}
		}

		// License
		if ( isset( $data['statements']['P275'][0] ) ) {
			$val = $data['statements']['P275'][0];
			$wdId = $val['mainsnak']['datavalue']['value']['id'] ?? null;
			if ( $wdId ) {
				// Clear any data in the license to avoid mismatches in multi-licensed works
				if ( count( $data['statements']['P275'] ) > 1 ) {
					$fd->license = new LicenseDescription();
				}
				$fd->license->wikidataId = $wdId;
			}
		}

		// Date
		$dateVal = $data['statements']['P571'][0]['mainsnak']['datavalue']['value'] ?? null;
		if ( $dateVal ) {
			$isoDate = $this->processDate( $dateVal );
			if ( $isoDate ) {
				$fd->dateTime = $isoDate;
			}
		}
	}

	/**
	 * Processes a Wikibase date value into an ISO-formatted string.
	 *
	 * @param array $dateValue
	 *
	 * @return string|null
	 */
	private function processDate( array $dateValue ) : ?string {
		if ( !preg_match(
			'/^\+(\d\d\d\d)-(\d\d)-(\d\d)T([\d:]+)/',
			$dateValue['time'] ?? '',
			$matches
		) ) {
			return null;
		}

		$precision = $dateValue['precision'];
		$year = $matches[1];

		// Precision of 9 is up to a year.
		// In such a case, day and month will be filled by zeros, which glitches out SMW.
		if ( $precision < 10 ) {
			return $year;
		}

		$month = $matches[2];
		if ( $precision < 11 )  {
			return "$year-$month";
		}

		$day = $matches[3];
		if ( $precision < 12 )  {
			return "$year-$month-$day";
		}

		$time = $matches[4];
		$tz = sprintf( '%02d', intval( $dateValue['timezone'] ?? '0' ) );

		return "$year-$month-$day $time+$tz:00";
	}
}
