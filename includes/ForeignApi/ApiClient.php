<?php

namespace SemanticCommonsClient\ForeignApi;

use SemanticCommonsClient\FileDescription\FileDescriptionSet;

interface ApiClient {
	public function getDataForFiles( FileDescriptionSet $files ) : void;
}