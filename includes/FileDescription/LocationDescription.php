<?php

namespace SemanticCommonsClient\FileDescription;

class LocationDescription extends FileDescriptionComponent {

	/** @var string */
	public $longitude;

	/** @var string */
	public $latitude;

	/**
	 * @inheritDoc
	 */
	public static function getSerializationFormatVersion() : string {
		return 1;
	}

	/**
	 * @inheritDoc
	 */
	public function setFromArray( array $a ) : void {
		$this->longitude = $a['longitude'];
		$this->latitude = $a['latitude'];
	}

	/**
	 * @inheritDoc
	 */
	public function jsonSerialize() {
		return [
			'longitude' => $this->longitude,
			'latitude' => $this->latitude,
		];
	}

	public function postprocess() : void {

	}
}