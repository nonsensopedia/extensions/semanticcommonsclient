<?php

namespace SemanticCommonsClient\FileDescription;

use BagOStuff;
use SemanticCommonsClient\Cache\SimpleCache;

class FileDescriptionSet extends SimpleCache {

	public const FILTER_UNCACHED_ONLY = 'uncached';
	public const FILTER_WITH_WIKIBASE_ID = 'wikibase';

	/**
	 * @var FileDescription[]
	 * Redeclared for proper type hinting.
	 */
	protected $set;

	/**
	 * @param BagOStuff $mainStash
	 * @param string[] $filenames Filenames in the form "File:XXX", normalized
	 */
	public function __construct( BagOStuff $mainStash, array $filenames ) {
		$fds = [];
		foreach ( $filenames as $filename ) {
			$fds[$filename] = new FileDescription( $filename );
		}
		parent::__construct( $mainStash, $fds );
	}

	/**
	 * @param string $filename
	 *
	 * @return FileDescription|null Null if not in set
	 */
	public function get( string $filename ) : ?FileDescription {
		return $this->set[$filename] ?? null;
	}

	/**
	 * @param string[] $filters Array of FILTER_* constants
	 *
	 * @return string[]
	 */
	public function getNames( array $filters = [] ) : array {
		return array_keys( $this->getFiltered( $filters ) );
	}

	/**
	 * @param string[] $filters
	 *
	 * @return FileDescription[]
	 */
	public function getValues( array $filters = [] ) : array {
		return array_values( $this->getFiltered( $filters ) );
	}

	/**
	 * @param string[] $filters Array of FILTER_* constants
	 *
	 * @return FileDescription[]
	 */
	private function getFiltered( array $filters ) : array {
		$result = $this->set;
		if ( in_array( self::FILTER_UNCACHED_ONLY, $filters ) ) {
			$result = array_filter(
				$result,
				function ( FileDescription $fd ) : bool {
					return !$fd->getCacheTimestamp();
				}
			);
		}
		if ( in_array( self::FILTER_WITH_WIKIBASE_ID, $filters ) ) {
			$result = array_filter(
				$result,
				function ( FileDescription $fd ) : bool {
					return isset( $fd->entityId );
				}
			);
		}

		return $result;
	}

	/**
	 * @return array
	 */
	public function getRaw() : array {
		return array_map(
			function ( FileDescription $fd ) {
				return $fd->jsonSerialize();
			},
			$this->set
		);
	}
}