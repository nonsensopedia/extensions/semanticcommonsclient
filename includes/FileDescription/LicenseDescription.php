<?php

namespace SemanticCommonsClient\FileDescription;

use SemanticCommonsClient\Cache\Cacheable;

class LicenseDescription extends FileDescriptionComponent implements Cacheable {

	private const CACHE_KEY_PREFIX = 'scc:license:';

	/** @var string */
	public $shortName;

	/** @var string */
	public $longName;

	/** @var string */
	public $url;

	/** @var string */
	public $wikidataId;

	/** @var string */
	public $spdxId;

	/** @var bool */
	public $isPublicDomain;

	/** @var string */
	private $cacheTimestamp;

	/**
	 * @inheritDoc
	 */
	public static function getSerializationFormatVersion() : string {
		return '2';
	}

	/**
	 * @inheritDoc
	 */
	public function setFromArray( array $a ) : void {
		$this->cacheTimestamp = $a['cacheTimestamp'] ?? null;
		$this->shortName = $a['shortName'];
		$this->longName = $a['longName'];
		$this->url = $a['url'];
		$this->wikidataId = $a['wikidataId'];
		$this->spdxId = $a['spdxId'];
		$this->isPublicDomain = $a['isPublicDomain'];
	}

	/**
	 * @inheritDoc
	 */
	public function jsonSerialize() {
		$res = [
			'shortName' => $this->shortName,
			'longName' => $this->longName,
			'url' => $this->url,
			'wikidataId' => $this->wikidataId,
			'spdxId' => $this->spdxId,
			'isPublicDomain' => $this->isPublicDomain,
		];
		if ( $this->cacheTimestamp ) {
			$res['cacheTimestamp'] = $this->cacheTimestamp;
		}
		return $res;
	}

	public function postprocess() : void {
		// This shouldn't really be needed, but... just in case.
		$this->url = $this->normalizeUrl( $this->url );
	}

	/**
	 * @inheritDoc
	 */
	public function getCacheKey() : string {
		if ( !isset( $this->cacheKey ) ) {
			$this->cacheKey = self::CACHE_KEY_PREFIX .
				self::getSerializationFormatVersion() . ':' .
				\Wikimedia\base_convert( sha1( $this->wikidataId ), 16, 36 );
		}
		return $this->cacheKey;
	}

	public function getCacheTimestamp() : ?string {
		return $this->cacheTimestamp;
	}

	public function setCacheTimestamp( string $timestamp ) : void {
		$this->cacheTimestamp = $timestamp;
	}
}