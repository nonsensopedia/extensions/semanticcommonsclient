<?php

namespace SemanticCommonsClient\FileDescription;

use JsonSerializable;
use RemexHtml\Serializer\Serializer;
use RemexHtml\Tokenizer\Tokenizer;
use RemexHtml\TreeBuilder\Dispatcher;
use RemexHtml\TreeBuilder\TreeBuilder;
use SemanticCommonsClient\SccHtmlFormatter;

abstract class FileDescriptionComponent implements JsonSerializable {

	/**
	 * Returns serialization format version string for this component.
	 *
	 * @return string
	 */
	public abstract static function getSerializationFormatVersion() : string;

	/**
	 * Sets data on this component from an array.
	 *
	 * @param array $a
	 */
	public abstract function setFromArray( array $a ) : void;

	/**
	 * Postprocesses the data in the component for user output.
	 */
	public abstract function postprocess() : void;

	/**
	 * Strips HTML from a string.
	 *
	 * @param string|null $text
	 *
	 * @return string
	 */
	protected function stripHtml( ?string $text ) : ?string {
		if ( !$text ) {
			return null;
		}

		$serializer = new Serializer( new SccHtmlFormatter() );
		$treeBuilder = new TreeBuilder(
			$serializer,
			[
				'ignoreErrors' => true,
				'ignoreNulls' => true
			]
		);
		$dispatcher = new Dispatcher( $treeBuilder );
		$tokenizer = new Tokenizer(
			$dispatcher,
			$text,
			[
				'ignoreErrors' => true,
				'skipPreprocess' => true,
				'ignoreNulls' => true
			]
		);
		$tokenizer->execute();
		$text = substr( $serializer->getResult(), strlen( '<!DOCTYPE html>' ) );
		return trim( html_entity_decode( $text ) );
	}

	/**
	 * "Normalizes" URLs, assuming Wikimedia Commons as the site when given a relative path.
	 *
	 * @param string|null $url
	 *
	 * @return string|null
	 */
	protected function normalizeUrl( ?string $url ) : ?string {
		if ( $url === null ) {
			return null;
		}

		if ( strpos( $url, '//' ) === 0 ) {
			// Use HTTPS for great justice.
			return 'https:' . $url;
		} elseif ( strpos( $url, '/' ) === 0 ) {
			// Attempt to convert a site-relative URL into an absolute one
			return 'https://commons.wikimedia.org' . $url;
		}
		return $url;
	}
}
