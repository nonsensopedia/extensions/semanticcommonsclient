<?php

namespace SemanticCommonsClient\FileDescription;

use BagOStuff;
use SemanticCommonsClient\Cache\SimpleCache;

/**
 * This thing is weird and probably has a wrong name.
 *
 * It ONLY handles licenses with Wikidata ID set, ignores any others.
 * Does caching of licenses.
 */
class LicenseDescriptionSet extends SimpleCache {

	public const FILTER_UNCACHED_ONLY = 'uncached';

	/**
	 * @var LicenseDescription[]
	 * Redeclared for proper type hinting
	 */
	protected $set;

	/**
	 * LicenseDescriptionSet constructor.
	 *
	 * @param BagOStuff $mainStash
	 * @param LicenseDescription[] $licenses
	 */
	public function __construct( BagOStuff $mainStash, array $licenses ) {
		$set = [];
		foreach ( $licenses as $license ) {
			if ( $license->wikidataId ) {
				$set[$license->wikidataId] = $license;
			}
		}
		parent::__construct( $mainStash, $set );
	}

	/**
	 * @param bool $uncachedOnly
	 *
	 * @return string[]
	 */
	public function getNames( bool $uncachedOnly = false ) : array {
		return array_keys( $this->getFiltered( $uncachedOnly ) );
	}

	/**
	 * @param bool $uncachedOnly
	 *
	 * @return LicenseDescription[]
	 */
	public function getValues( bool $uncachedOnly = false ) : array {
		return array_values( $this->getFiltered( $uncachedOnly ) );
	}

	/**
	 * @param string $wikidataId
	 *
	 * @return LicenseDescription|null
	 */
	public function getById( string $wikidataId ) : ?LicenseDescription {
		return $this->set[$wikidataId] ?? null;
	}

	/**
	 * @param bool $uncachedOnly
	 *
	 * @return LicenseDescription[]
	 */
	private function getFiltered( bool $uncachedOnly ) : array {
		if ( $uncachedOnly ) {
			return array_filter(
				$this->set,
				function ( LicenseDescription $license ) : bool {
					return !$license->getCacheTimestamp();
				}
			);
		}

		return $this->set;
	}
}