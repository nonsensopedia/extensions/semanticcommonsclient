<?php

namespace SemanticCommonsClient\FileDescription;

class AuthorDescription extends FileDescriptionComponent {

	/** @var string */
	public $text;

	/** @var string */
	public $wikimediaUsername;

	/** @var string */
	public $url;

	/** @var string */
	public $wikipediaArticleTitle;

	/** @var string */
	public $wikipediaArticleLang;

	/** @var string */
	public $wikidataId;

	/** @var bool */
	public $isAnonymous;

	/**
	 * @inheritDoc
	 */
	public static function getSerializationFormatVersion() : string {
		return '2';
	}

	/**
	 * @inheritDoc
	 */
	public function setFromArray( array $a ) : void {
		$this->text = $a['text'];
		$this->wikimediaUsername = $a['wikimediaUsername'];
		$this->url = $a['url'];
		$this->wikipediaArticleTitle = $a['wikipediaArticleTitle'];
		$this->wikipediaArticleLang = $a['wikipediaArticleLang'];
		$this->wikidataId = $a['wikidataId'];
		$this->isAnonymous = $a['isAnonymous'];
	}

	/**
	 * @inheritDoc
	 */
	public function jsonSerialize() {
		return [
			'text' => $this->text,
			'wikimediaUsername' => $this->wikimediaUsername,
			'url' => $this->url,
			'wikipediaArticleTitle' => $this->wikipediaArticleTitle,
			'wikipediaArticleLang' => $this->wikipediaArticleLang,
			'wikidataId' => $this->wikidataId,
			'isAnonymous' => $this->isAnonymous,
		];
	}

	public function postprocess() : void {
		if ( $this->isAnonymous === null ) {
			if ( $this->wikidataId || $this->url || $this->wikimediaUsername ) {
				$this->isAnonymous = false;
			}
		}
		$this->url = $this->normalizeUrl( $this->url );
		$this->text = self::stripHtml( $this->text );
	}
}