<?php

namespace SemanticCommonsClient\FileDescription;

use SemanticCommonsClient\Cache\Cacheable;

class FileDescription extends FileDescriptionComponent implements Cacheable {

	private const CACHE_KEY_PREFIX = 'scc:filedescription:';

	private const COMPONENTS = [
		'location' => LocationDescription::class,
		'author' => AuthorDescription::class,
		'license' => LicenseDescription::class,
	];

	/** @var bool */
	public $found;

	/** @var string */
	public $entityId;

	/** @var string */
	public $dateTime;

	/** @var string */
	public $caption;

	/** @var LocationDescription */
	public $location;

	/** @var AuthorDescription */
	public $author;

	/** @var LicenseDescription */
	public $license;

	/** @var string[] */
	private $warnings;

	/** @var string */
	private $filename;

	/** @var string */
	private $cacheKey;

	/** @var string */
	private $cacheTimestamp;

	public static function getSerializationFormatVersion() : string {
		$versionVector = [ 2 ]; // top array version
		foreach ( self::COMPONENTS as $class ) {
			$versionVector[] = call_user_func( [ $class, 'getSerializationFormatVersion' ] );
		}
		return implode( '.', $versionVector );
	}

	/**
	 * @param string $filename Filename in the form "File:XXX"
	 */
	public function __construct( string $filename ) {
		$this->filename = $filename;
		$this->found = false;
		$this->warnings = [];
		$this->location = new LocationDescription();
		$this->author = new AuthorDescription();
		$this->license = new LicenseDescription();
	}

	/**
	 * @return string
	 */
	public function getFilename() : string {
		return $this->filename;
	}

	/**
	 * @inheritDoc
	 */
	public function getCacheKey() : string {
		if ( !isset( $this->cacheKey ) ) {
			$this->cacheKey = self::CACHE_KEY_PREFIX .
				self::getSerializationFormatVersion() . ':' .
				\Wikimedia\base_convert( sha1( $this->filename ), 16, 36 );
		}
		return $this->cacheKey;
	}

	/**
	 * Returns an array of warnings. Each has the following keys:
	 * * code => error message key
	 * * params => array of message parameters
	 *
	 * @return string[]
	 */
	public function getWarnings() : array {
		return $this->warnings;
	}

	/**
	 * @param string $messageKey
	 * @param array ...$params
	 */
	public function addWarning( string $messageKey, array ...$params ) : void {
		$this->warnings[] = [
			'code' => $messageKey,
			'params' => $params
		];
	}

	/**
	 * @param array $a
	 *
	 * @return FileDescription
	 */
	public static function newFromArray( array $a ) : FileDescription {
		$fd = new self( $a['filename'] );
		$fd->setFromArray( $a );
		return $fd;
	}

	public function setFromArray( array $a ) : void {
		$this->cacheTimestamp = $a['cacheTimestamp'];
		$this->found = $a['found'];
		$this->entityId = $a['entityId'];
		$this->dateTime = $a['dateTime'];
		$this->caption = $a['caption'];
		$this->warnings = $a['warnings'];

		foreach ( self::COMPONENTS as $name => $class ) {
			$this->$name = new $class();
			/** @var FileDescriptionComponent $comp */
			$comp = $this->$name;
			$comp->setFromArray( $a[$name] );
		}
	}

	/**
	 * Postprocesses the data by stripping HTML and setting some previously
	 * maybe unset properties.
	 */
	public function postprocess() : void {
		$this->dateTime = self::stripHtml( $this->dateTime );

		foreach ( self::COMPONENTS as $name => $class ) {
			/** @var FileDescriptionComponent $comp */
			$comp = $this->$name;
			$comp->postprocess();
		}
	}

	/**
	 * @return array
	 */
	public function jsonSerialize() {
		$result = [
			'filename' => $this->filename,
			'cacheTimestamp' => $this->cacheTimestamp,
			'found' => $this->found,
			'entityId' => $this->entityId,
			'dateTime' => $this->dateTime,
			'caption' => $this->caption,
			'warnings' => $this->warnings,
		];
		foreach ( self::COMPONENTS as $name => $class ) {
			/** @var FileDescriptionComponent $comp */
			$comp = $this->$name;
			$result[$name] = $comp->jsonSerialize();
		}
		return $result;
	}

	public function getCacheTimestamp() : ?string {
		return $this->cacheTimestamp;
	}

	public function setCacheTimestamp( string $timestamp ) : void {
		$this->cacheTimestamp = $timestamp;
	}
}