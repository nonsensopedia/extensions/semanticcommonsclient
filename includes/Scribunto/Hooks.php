<?php

namespace SemanticCommonsClient\Scribunto;

class Hooks {

	public static function onScribuntoExternalLibraries(
		string $engine, array &$extraLibraries
	) : bool {
		$extraLibraries['mw.commonsClient'] = SccLuaLibrary::class;
		return true;
	}
}