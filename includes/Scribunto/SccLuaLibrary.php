<?php

namespace SemanticCommonsClient\Scribunto;

use File;
use MediaWiki\MediaWikiServices;
use Scribunto_LuaLibraryBase;
use SemanticCommonsClient\Services\SccServices;
use Title;

class SccLuaLibrary extends Scribunto_LuaLibraryBase {

	/**
	 * Register the library.
	 */
	public function register() : void {
		$lib = [
			'getData' => [ $this, 'getData' ],
			'getAuthorData' => [ $this, 'getAuthorData' ],
			'getFileRepo' => [ $this, 'getFileRepo' ],
		];

		$this->getEngine()->registerInterface(
			__DIR__ . '/../../resources/lua/mw.ext.commonsClient.lua',
			$lib,
			[]
		);
	}

	/**
	 * @param string[] $filenames
	 * @param string $useWikibase
	 * @param string $useWikidata
	 *
	 * @return array
	 */
	public function getData(
		array $filenames,
		string $useWikibase,
		string $useWikidata
	) : array {
		// Yeah, that whole Lua thing does not support DI :<
		$fds = SccServices::getFileDescriptionService();
		$targets = [];
		foreach ( $filenames as $filename ) {
			if ( !is_string( $filename ) ) {
				continue; // ignore
			}
			$target = Title::newFromText( $filename );
			if ( $target !== null ) {
				$targets[] = $target;
			}
		}

		$data = $fds->getDataForFiles(
			$targets,
			false,
			self::luaIsAwful( $useWikibase ),
			self::luaIsAwful( $useWikidata )
		);

		foreach ( $data as $key => $row ) {
			if ( ( $row['location']['latitude'] ?? null ) === null ) {
				unset( $data[$key]['location'] );
			}
			if ( count( $row['warnings'] ) === 0 ) {
				unset( $data[$key]['warnings'] );
			}
		}

		return [ $data ];
	}

	/**
	 * @param string[] $names
	 *
	 * @return array
	 */
	public function getAuthorData( array $names ) : array {
		if ( count( $names ) === 0 ) {
			return [ [] ];
		}

		$ads = SccServices::getAuthorDescriptionService();
		$data = $ads->getDataForAuthors( $names, false );

		return [ $data ];
	}

	/**
	 * @param string $x
	 *
	 * @return bool
	 */
	private static function luaIsAwful( string $x ) : bool {
		return $x === 'true';
	}

	/**
	 * @param string[] $filenames
	 */
	public function getFileRepo( array $filenames ) : array {
		$titles = [];
		foreach ( $filenames as $filename ) {
			// Strip any NS or IW prefix
			$title = Title::newFromText( $filename );
			if ( $title === null ) continue;
			$titles[] = Title::newFromText( $title->getText(), NS_FILE );
		}

		if ( !$titles ) {
			return [ [] ];
		}
		$repoGroup = MediaWikiServices::getInstance()->getRepoGroup();

		$result = [];
		foreach ( $repoGroup->findFiles( $titles ) as $key => $file ) {
			/** @var File $file */
			$result[$key] = $file->getRepoName();
		}
		return [ $result ];
	}
}