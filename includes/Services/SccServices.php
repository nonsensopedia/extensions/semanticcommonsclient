<?php

namespace SemanticCommonsClient\Services;

use MediaWiki\MediaWikiServices;

class SccServices {

	public static function getFileDescriptionService() : FileDescriptionService {
		return MediaWikiServices::getInstance()->getService( 'SccFileDescriptionService' );
	}

	public static function getAuthorDescriptionService() : AuthorDescriptionService {
		return MediaWikiServices::getInstance()->getService( 'SccAuthorDescriptionService' );
	}
}