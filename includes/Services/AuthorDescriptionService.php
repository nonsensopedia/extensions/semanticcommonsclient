<?php

namespace SemanticCommonsClient\Services;

use BagOStuff;
use SemanticCommonsClient\ConfigHacks\Configurable;
use SemanticCommonsClient\ConfigHacks\SaneServiceOptions;
use SemanticCommonsClient\FileDescription\AuthorDescription;
use SemanticCommonsClient\ForeignApi\WikidataSparqlClient;

class AuthorDescriptionService {
	use Configurable;

	private const CACHE_KEY_PREFIX = 'scc:authordata:';

	public static function getConstructorOptions() : array {
		return WikidataSparqlClient::getConstructorOptions();
	}

	/** @var BagOStuff */
	private $mainStash;

	/** @var SaneServiceOptions */
	private $options;

	public function __construct(
		BagOStuff $mainStash,
		SaneServiceOptions $options
	) {
		self::assertRequiredOptions( $options );

		$this->options = $options;
		$this->mainStash = $mainStash;
	}

	/**
	 * @param string[] $targets
	 * @param bool $ignoreCache
	 *
	 * @return array
	 */
	public function getDataForAuthors(
		array $targets,
		bool $ignoreCache
	) : array {
		$result = [];

		if ( !$ignoreCache ) {
			// Attempt to retrieve data from cache
			$cacheKeys = [];
			foreach ( $targets as $target ) {
				$cacheKeys[$this->getCacheKey( $target )] = $target;
			}

			$fromCache = $this->mainStash->getMulti( array_keys( $cacheKeys ) );
			foreach ( $fromCache as $key => $val ) {
				$result[$cacheKeys[$key]] = $val;
				unset( $cacheKeys[$key] );
			}
		} else {
			$cacheKeys = $targets;
		}

		// Obtain the rest
		$wsc = new WikidataSparqlClient(
			$this->mainStash,
			WikidataSparqlClient::buildServiceOptions( $this->options )
		);
		$queryResult = $wsc->getAuthorData( array_values( $cacheKeys ) );

		$toCache = [];
		foreach ( $queryResult as $key => $authorDescription ) {
			$authorDescription->postprocess();
			$raw = $authorDescription->jsonSerialize();
			$result[$key] = $raw;

			$raw['cacheTimestamp'] = wfTimestampNow();
			$toCache[$this->getCacheKey( $key )] = $raw;
		}

		$this->mainStash->setMulti( $toCache, BagOStuff::TTL_DAY * 7, BagOStuff::WRITE_BACKGROUND );

		return $result;
	}

	/**
	 * @param string $baseKey
	 *
	 * @return string
	 */
	public static function getCacheKey( string $baseKey ) : string {
		return self::CACHE_KEY_PREFIX .
			AuthorDescription::getSerializationFormatVersion() . ':' .
			\Wikimedia\base_convert( sha1( $baseKey ), 16, 36 );
	}
}