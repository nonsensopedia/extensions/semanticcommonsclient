<?php

namespace SemanticCommonsClient\Services;

use BagOStuff;
use MediaWiki\Http\HttpRequestFactory;
use MediaWiki\Linker\LinkTarget;
use SemanticCommonsClient\ConfigHacks\Configurable;
use SemanticCommonsClient\ConfigHacks\SaneServiceOptions;
use SemanticCommonsClient\FileDescription\FileDescriptionSet;
use SemanticCommonsClient\ForeignApi\CommonsApiClient;
use SemanticCommonsClient\ForeignApi\CommonsWikibaseClient;
use SemanticCommonsClient\ForeignApi\WikidataSparqlClient;

class FileDescriptionService {
	use Configurable;

	public static function getConstructorOptions() : array {
		return array_unique( array_merge(
			CommonsApiClient::getConstructorOptions(),
			CommonsWikibaseClient::getConstructorOptions(),
			WikidataSparqlClient::getConstructorOptions()
		) );
	}

	/** @var BagOStuff */
	private $mainStash;

	/** @var HttpRequestFactory */
	private $httpRequestFactory;

	/** @var SaneServiceOptions */
	private $options;

	/**
	 * FileDescriptionService constructor.
	 *
	 * @param HttpRequestFactory $httpRequestFactory
	 * @param BagOStuff $mainStash
	 * @param SaneServiceOptions $options
	 */
	public function __construct(
		HttpRequestFactory $httpRequestFactory,
		BagOStuff $mainStash,
		SaneServiceOptions $options
	) {
		self::assertRequiredOptions( $options );

		$this->options = $options;
		$this->mainStash = $mainStash;
		$this->httpRequestFactory = $httpRequestFactory;
	}

	/**
	 * @param LinkTarget[] $targets
	 * @param bool $ignoreCache
	 * @param bool $useWikibase
	 * @param bool $useWikidata
	 *
	 * @return array
	 */
	public function getDataForFiles(
		array $targets,
		bool $ignoreCache = false,
		bool $useWikibase = true,
		bool $useWikidata = true
	) : array {
		$filenames = array_map(
			function ( LinkTarget $lt ) {
				return 'File:' . $lt->getText();
			},
			$targets
		);
		$fds = new FileDescriptionSet( $this->mainStash, $filenames );

		if ( !$ignoreCache && $fds->loadCached() === count( $targets ) ) {
			// All data was loaded from cache
			return $fds->getRaw();
		}

		$cac = new CommonsApiClient(
			$this->httpRequestFactory,
			CommonsApiClient::buildServiceOptions( $this->options )
		);
		$cac->getDataForFiles( $fds );

		if ( $useWikibase ) {
			$cwc = new CommonsWikibaseClient(
				$this->httpRequestFactory,
				CommonsWikibaseClient::buildServiceOptions( $this->options )
			);
			$cwc->getDataForFiles( $fds );
		}

		if ( $useWikidata ) {
			$wsc = new WikidataSparqlClient(
				$this->mainStash,
				WikidataSparqlClient::buildServiceOptions( $this->options ),
				!$ignoreCache
			);
			$wsc->getDataForFiles( $fds );
		}

		foreach ( $fds->getValues( [ FileDescriptionSet::FILTER_UNCACHED_ONLY ] ) as $fd ) {
			$fd->postprocess();
		}

		$fds->saveToCache();
		return $fds->getRaw();
	}
}