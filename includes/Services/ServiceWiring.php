<?php

use MediaWiki\MediaWikiServices;
use SemanticCommonsClient\Services\AuthorDescriptionService;
use SemanticCommonsClient\Services\FileDescriptionService;

return [
	'SccFileDescriptionService' => function ( MediaWikiServices $services ) : FileDescriptionService {
		return new FileDescriptionService(
			$services->getHttpRequestFactory(),
			$services->getMainObjectStash(),
			FileDescriptionService::buildServiceOptions( $services->getMainConfig() )
		);
	},
	'SccAuthorDescriptionService' => function ( MediaWikiServices $services ) : AuthorDescriptionService {
		return new AuthorDescriptionService(
			$services->getMainObjectStash(),
			AuthorDescriptionService::buildServiceOptions( $services->getMainConfig() )
		);
	},
];
