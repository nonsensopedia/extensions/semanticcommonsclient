local scc = {}
local php

-- setup magic, don't ask me
function scc.setupInterface()
    scc.setupInterface = nil
    php = mw_interface
    mw_interface = nil

    mw = mw or {}
    mw.ext = mw.ext or {}
    mw.ext.commonsClient = scc
    package.loaded['mw.ext.commonsClient'] = scc
end

local function asPseudoBool(value, default )
    if default ~= nil and value == nil then
        return default
    end

    if value then
        return 'true'
    else
        return 'false'
    end
end

local function stringAsArrayOrError( value )
    if type( value ) == 'string' then
        return { value }
    elseif not type( value ) == 'table' then
        error( 'Invalid argument type, a table or string was expected' )
    end

    return value
end

---getData
---@param filenames table
---@param use_wikibase boolean
---@param use_wikidata boolean
function scc.getData( filenames, use_wikibase, use_wikidata )
    return php.getData(
        stringAsArrayOrError( filenames ),
        asPseudoBool( use_wikibase, 'true' ),
        asPseudoBool( use_wikidata, 'true' )
    )
end

---getAuthorData
---@param names table Either Wikidata IDs (eg. 'Q42') or interwiki Wikipedia links
function scc.getAuthorData( names )
    return php.getAuthorData( stringAsArrayOrError( names ) )
end

---getFileRepo
---@param filenames table
function scc.getFileRepo( filenames )
    return php.getFileRepo( stringAsArrayOrError( filenames ) )
end

return scc